# distfiles_updater

This repo is deprecated and has been replaced by the [dupit recipe](https://source.chromium.org/chromiumos/chromiumos/codesearch/+/master:infra/recipes/recipes/dupit.py), which runs via the [DupIt LUCI Scheduler job](https://luci-scheduler.appspot.com/jobs/chromeos/DupIt).
